### CodeShare
##### Code Solutions Project

FEATURES
-------------------
-------------------
* Hosts code snippets to solve common problems in software programming
* Supports 39 languages between general programming (Java, C ...), scripting (Perl, Bash), web design (HTML...,Twig...,Sass...), data serialization (JSON,YAML...) and more
* Follows material design guidelines
* Use bootstrap 3 by Twitter
* Upload in [codeshare.jkanetwork.com](http://codeshare.jkanetwork.com)

External OpenSource
------------------
* [highlight.js](https://highlightjs.org/) by Ivan Sagalev licensed under BSD

License
-------
* APACHE 2.0