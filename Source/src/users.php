<?php
/**
 * Created by PhpStorm.
 * User: joselucross
 * Date: 9/07/17
 * Time: 19:06
 */

function checkInfo($request){
    $email = $request->get('email');
    $emailre = $request->get('emailre');
    if ($email != null)
        return 1;
    else if ($emailre != null)
        return 2;
    else
        return 3;
}

/**
 * Check if login is correct
 *
 * @param $email user's input email
 * @param $pass user's input password
 * @return bool true if email-password combination is correct, false if not
 */
function checklogin($email,$pass,DB $db){
    $bool = $db->checkPass($email,$pass);
    if($bool){
        $idu = $db->loadIDU($email);
        $ROLE = $db->loadProfile($idu)['ROLE'];
        if($ROLE==0)
            return 'VALIDATE';
        $token = RandomString(50);
        $db->setToken($idu,$token);
        $_SESSION['token'] = $token;
        $_SESSION['sessionID'] =$idu;
        return '';
    }
    return 'BAD';
}

/**
 * Register a new user in platform
 *
 * @param $email new user's email
 * @param $pass new user's password
 * @param $nick new user's nickname
 * @return bool true if email never exists before, else false
 */
function register($email,$emailre,$pass,$passre,$nick,$db){

    if($emailre != $email || $pass != $passre)
        return 'POST_ERROR';
    $state = $db->register($email,$pass,$nick);
    if($state == 'CORRECT') {
        return "";
    }else {
        return $state;
    }

}

/**
 * Logout user
 */
function logout(){
    session_destroy(); //Logout
}

function getUser($app){
    $user=null;
    if(isset($_SESSION['sessionID'])){
        if($app['data']->checkCookie($_SESSION['sessionID'],$_SESSION['token'])){
            $user = $app['data']->loadProfile($_SESSION['sessionID']);
        }
    }
    return $user;
}