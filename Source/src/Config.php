<?php

/**
 * Class Config
 *
 * Project configuration static class
 */
class Config
{

    public static $config;
    private static $in=false;

    /**
     * Initialize project
     */
    public static function init(){
        if(!Config::initialized()) {
            $file = file_get_contents(__DIR__."/../data/project.json");
            Config::$config = json_decode($file, true);
            Config::$in=true;
        }
    }

    /**
     * Get if project is initialize
     *
     * @return bool
     */
    public static function initialized(){
        return Config::$in;
    }

    /**
     * Get lang
     *
     * @return string with lang (es, en, fr...)
     */
    public static function getLang(){
        if(!Config::initialized())
            Config::init();
        return Config::$config["lang"];
    }

    /**
     * Get the project name
     *
     * @return string with the name
     */
    public static function getProject(){
        if(!Config::initialized())
            Config::init();
        return Config::$config["project"];
    }

    /**
     * Get the master of this project. For example, ThiefBusters belongs SoftwareTalent
     *
     * @return string with master
     */
    public static function getMaster(){
        if(!Config::initialized())
            Config::init();
        return Config::$config["master"];
    }

    /**
     * Get the company, usually "Code Solutions Project"
     *
     * @return string whit company
     */
    public static function getCompany(){
        if(!Config::initialized())
            Config::init();
        return Config::$config["company"];
    }

    /**
     * Get the DB Connection
     *
     * @return mixed db conn
     */
    public static function getDBConexion(){
        if(!Config::initialized())
            Config::init();
        $DB = Config::$config["database"];
        return dbw_connect($DB["SGBD"], $DB["path"], $DB["db"], $DB["user"], $DB["password"]);
    }

    public static function getDBalConexion(){
        if(!Config::initialized())
            Config::init();
        $DB = Config::$config["database"];
        return array(
            'dbname'   => $DB['db'],
            'user'     => $DB['user'],
            'password' => $DB['password'],
            'host'     => $DB['path'],
            'driver'   => "pdo_".$DB['SGBD']);
    }

    /**
     * Get options of project if exists
     *
     * @return array array whit options
     */
    public static function getOptions(){
        if(!Config::initialized())
            Config::init();
        return Config::$config["options"];
    }

}