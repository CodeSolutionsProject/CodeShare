<?php

function newUser($email,$nick,$token){
    global $app;

    $header = "From: CodeShare <contacto@jkanetwork.com>\r\n";
    $header .= "MIME-Version: 1.0\r\n";
    $header .= "Content-Type: text/html; charset=UTF-8";
    $to = $email;
    $url = $app['url_generator']->generate('confirm',array('nick'=>$nick,'token'=>$token));
    $correo = "<p>Wellcome $nick to CodeShare, the distribution platform for code fragments</p>
               <p>To confirm your account please, click in the next link: <a href='https://codeshare.jkanetwork.com$url'>https://codeshare.jkanetwork.com$url</a></p>";
    mail($to,"Confirm account to $nick",$correo,$header);
}

function restorePassword($email,$nick,$token,$timestamp){
    global $app;

    $header = "From: CodeShare <contacto@jkanetwork.com>\r\n";
    $header .= "MIME-Version: 1.0\r\n";
    $header .= "Content-Type: text/html; charset=UTF-8";
    $to = $email;
    $url = $app['url_generator']->generate('restorePass',array('nick'=>$nick,'token'=>$token,'timestamp' => $timestamp));
    $correo = "<p>Someone requested that the password be reset for you</p>
               <p>If this was a mistake, just ignore this email and nothing will happen.</p>
               <p>To restore your password please, click in the next link: <a href='https://codeshare.jkanetwork.com$url'>https://codeshare.jkanetwork.com$url</a></p>";
    mail($to,"Restore password to $nick",$correo,$header);
}