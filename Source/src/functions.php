<?php
/**
 * Created by PhpStorm.
 * User: joselucross
 * Date: 6/07/17
 * Time: 11:56
 */

/**
 * Generate a RandomString
 *
 * @param $length length of string
 * @return string string generated
 */
function RandomString($length)
{
    //https://phpes.wordpress.com/2007/06/12/generador-de-una-cadena-aleatoria/
    $source = 'abcdefghijklmnopqrstuvwxyz';
    $source .= '1234567890';
    if($length>0){
        $rstr = "";
        $source = str_split($source,1);
        for($i=1; $i<=$length; $i++){
            mt_srand((double)microtime() * 1000000);
            $num = mt_rand(1,count($source));
            $rstr .= $source[$num-1];
        }

    }
    return $rstr;
}

/**
 * Parse a file from Keys system object notation (KSON) to Mapped array
 *
 * @param $path path to file
 * @return array array from kson
 */
function ksonParse($path){
    $file = fopen($path,"r");
    $array = [];
    if($file){
        while(!feof($file)){
            $line = fgets($file);
            $part = explode(":",$line);
            $array[$part[0]]=$part[1];
        }
        return $array;
    }else{
        die('<h3>Error, kson file not exist</h3>');
    }
}

/**
 * Group by $supported 
 */
function groupByCategory($supported){
    $grouped = Array();
    foreach($supported as $key => $val){
        if(!array_key_exists($val[1],$grouped)){
            switch($val[1]){
                case "Programming":
                    $num=5;
                    break;
                case "Config":
                case "Markup":
                    $num=2;
                    break;
                case "Script":
                case "Data":
                case "Mathematics":
                    $num=1;
                    break;
            }
            $grouped[$val[1]] = Array(Array(),$num);
        }
        array_push($grouped[$val[1]][0],[$key,$val[0]]);
    }
    return $grouped;
}

function checkCaptcha($response){
    $url = "https://www.google.com/recaptcha/api/siteverify";
    //$post = 'secret='.'6Ld-C3cUAAAAAC6NhPw-rn4LsdM2PjKg255H6j0o&response='. $response;
    $post = array('secret'   => "6Ld-C3cUAAAAAC6NhPw-rn4LsdM2PjKg255H6j0o",
                  'response' => $response);

    $options = array(
        'http' => array(
            "header" => "Content-type: application/x-www-form-urlencoded\r\n",
            "method" => 'POST',
            'content'=> http_build_query($post)
        )
    );
    $context = stream_context_create($options);
    $response = file_get_contents($url,false,$context);

    return json_decode($response,true)["success"];
}