<?php

class DB
{

    /*
     * DB Basic
     */

    /**
     * @var bool connection with database
     */
    private $conn;

    /**
     * DB constructor
     *
     * @param Doctrine\DBAL\Connection Connection of database
     */
    function __construct(Doctrine\DBAL\Connection $db)
    {
        $this->conn = $db;
        $this->createTable();
    }

    /*Base methods*/
    public function newQueryBuilder(){
        $queryBuilder = new \Doctrine\DBAL\Query\QueryBuilder($this->conn);
        return $queryBuilder;
    }

    public function execute(Doctrine\DBAL\Query\QueryBuilder $queryBuilder){
        $query = $queryBuilder->execute();
        return $query;
    }

    public function getData($queryBuilder){

        return $this->execute($queryBuilder)->fetchAll();
    }
    /*End methods*/

    /* Create Tables */

    /**
     * Create table if not exist in deploy (Database must be exist).
     */
    private function createTable()
    {
        $schema = $this->conn->getSchemaManager();
        $this->createUsers($schema);
        $this->createCodes($schema);
        $this->createSources($schema);
        $this->createLikes($schema);
        $this->createSaves($schema);
    }

    private function createUsers(Doctrine\DBAL\Schema\AbstractSchemaManager $schema){
        if(!$schema->tablesExist(array('Users'))){
            $users = new Doctrine\DBAL\Schema\Table("Users");

            $users->addColumn('IDU','integer',array('unsigned' => true,'autoincrement' => true));
            $users->addColumn('email','string',array('length' => 64));
            $users->addColumn('pass','string',array('length' => 64));
            $users->addColumn('nick','string',array('length' => 40));
            $users->addColumn('token','string',array('length' => 64));
            $users->addColumn('ROLE','string',array('length' => 10));

            $users->setPrimaryKey(array('IDU'));
            $users->addUniqueIndex(array('email'));
            $users->addUniqueIndex(array('nick'));

            $schema->createTable($users);
        }
    }

    private function createCodes(Doctrine\DBAL\Schema\AbstractSchemaManager $schema){
        if(!$schema->tablesExist(array('Codes'))){
            $codes = new \Doctrine\DBAL\Schema\Table('Codes');

            $codes->addColumn('IDC','integer',array('unsigned' => true, 'autoincrement' => true));
            $codes->addColumn('Name','string',array('length'=>80));
            $codes->addColumn('Description',"text");
            $codes->addColumn('Input',"text");
            $codes->addColumn('Output',"text");
            $codes->addColumn('UserCreator',"integer",array('unsigned' => true));

            $codes->setPrimaryKey(array('IDC'));
            $codes->addForeignKeyConstraint("Users", array('UserCreator'), array('IDU'));
            $codes->addIndex(array('UserCreator'));

            $schema->createTable($codes);
        }
    }

    private function createSources(Doctrine\DBAL\Schema\AbstractSchemaManager $schema){
        if(!$schema->tablesExist(array('Sources'))){
            $sources = new Doctrine\DBAL\Schema\Table('Sources');

            $sources->addColumn('IDC','integer',array('unsigned' => true, 'autoincrement' => true));
            $sources->addColumn('IDU','integer',array('unsigned' => true));
            $sources->addColumn('Lang','string',array('length' => 15));
            $sources->addColumn('Version','integer', array('unsigned' => true));
            $sources->addColumn('Modification','integer', array('unsigned' => true));
            $sources->addColumn('Code',"text");
            $sources->addColumn('UseExtLib',"text",array("notnull" => false));
            $sources->addColumn('UseExtLibVer',"string",array("length"=>55,"notnull" => false));

            $sources->setPrimaryKey(array('IDC','Lang','Version'));
            $sources->addIndex(array('IDU'));
            $sources->addForeignKeyConstraint('Users',array('IDU'),array('IDU'));
            $sources->addForeignKeyConstraint('Codes',array('IDC'),array('IDC'),array('onUpdate'=>'CASCADE','onDelete'=>'CASCADE'));

            $schema->createTable($sources);
        }
    }

    private function createLikes(\Doctrine\DBAL\Schema\AbstractSchemaManager $schema){
        if(!$schema->tablesExist(array('Likes'))){
            $like = new Doctrine\DBAL\Schema\Table('Likes');

            $like->addColumn('IDC','integer',array('unsigned' => true));
            $like->addColumn('Lang','string',array('length' => 15));
            $like->addColumn('IDU','integer'    ,array('unsigned'=>true));

            $like->addIndex(array('IDU'));
            $like->addIndex(array('IDC','Lang'));
            $like->addForeignKeyConstraint('Users',array('IDU'),array('IDU'),array('onUpdate'=>'CASCADE','onDelete'=>'CASCADE') );
            $like->addForeignKeyConstraint('Sources',array('IDC','Lang'),array('IDC','Lang'),array('onUpdate'=>'CASCADE','onDelete'=>'CASCADE'));
            $like->setPrimaryKey(array('IDC','Lang','IDU'));

            $schema->createTable($like);
        }
    }

    private function createSaves(\Doctrine\DBAL\Schema\AbstractSchemaManager $schema){
        if(!$schema->tablesExist(array('Saves'))){
            $save = new Doctrine\DBAL\Schema\Table('Saves');

            $save->addColumn('IDC','integer',array('unsigned' => true));
            $save->addColumn('Lang','string',array('length' => 15));
            $save->addColumn('IDU','integer'    ,array('unsigned'=>true));

            $save->addIndex(array('IDU'));
            $save->addIndex(array('IDC','Lang'));
            $save->addForeignKeyConstraint('Users',array('IDU'),array('IDU'),array('onUpdate'=>'CASCADE','onDelete'=>'CASCADE'));
            $save->addForeignKeyConstraint('Sources',array('IDC','Lang'),array('IDC','Lang'),array('onUpdate'=>'CASCADE','onDelete'=>'CASCADE'));
            $save->setPrimaryKey(array('IDC','Lang','IDU'));

            $schema->createTable($save);
        }
    }

    /*
     * Code - Source
     */

    //SQL SELECT

    /**
     * Load the user creator of code, is needed for know if the logged user is the same of creator.
     *
     * @param $IDC Code identifier
     * @return int User creator id
     */
    public function loadOriginalAuthor($IDC)
    {
        $queryBuilder = $this->newQueryBuilder();
        $queryBuilder
            ->select('UserCreator')
            ->from('Codes')
            ->where($queryBuilder->expr()->eq(
                'IDC','?'
            ))
            ->setParameter(0,$IDC);
        return $this->getData($queryBuilder)[0]['UserCreator'];

    }

    /**
     * Select all from a Snippet.
     *
     * @param $id code identifier
     * @param $lang lang of snippet
     * @param $version version of snippet
     * @return array all data from snippet specified
     */
    public function loadAll($id, $lang, $version)
    {
        $queryBuilder = $this->newQueryBuilder();
        $queryBuilder
            ->select('*')
            ->from('Users','u')
            ->join('u','Sources','s',
                $queryBuilder->expr()->eq(
                    'u.IDU','s.IDU'
                ))
            ->join('s','Codes','c',
                $queryBuilder->expr()->eq(
                    's.IDC','c.IDC'
                ))
            ->where($queryBuilder->expr()->andX(
                $queryBuilder->expr()->eq('c.IDC','?'),
                $queryBuilder->expr()->eq('s.Lang','?'),
                $queryBuilder->expr()->eq('s.Version','?')
            ))
            ->setParameter(0,$id)
            ->setParameter(1,$lang)
            ->setParameter(2,$version);
        $data = $this->getData($queryBuilder)[0];
        return $data;

    }

    /**
     * Load the last codes uploaded.
     *
     * @return mysqli_result the las 10 snippets hosted
     */
    public function loadLast($page = 0)
    {
        $each=10;
        $page=$page*$each;
        $queryBuilder = $this->newQueryBuilder();
        $subQuery = $this->newQueryBuilder();
        $queryBuilder
            ->select("c.IDC","Name","nick","Lang","Description","Code","Version")
            ->from('Users','u')
            ->join('u','Sources','s',
                $queryBuilder->expr()->eq(
                    'u.IDU','s.IDU'
                ))
            ->join('s','Codes','c',
                $queryBuilder->expr()->eq(
                    's.IDC','c.IDC'
                ))
            ->where(
                $queryBuilder->expr()->eq(
                    's.Version','('.
                    $subQuery
                        ->select('MAX(Version)')
                        ->from('Sources')
                        ->where(
                            $subQuery->expr()->andX(
                                $subQuery->expr()->eq('s.Lang','Lang'),
                                $subQuery->expr()->eq('s.IDC','IDC')
                            ))
                        ->getSql()
                    .')'
                ))
            ->orderBy('Modification','DESC')
            ->setFirstResult($page)
            ->setMaxResults(10);
            
        return $this->execute($queryBuilder);
    }

    /**
     * Load other versions of solution.
     *
     * @param $id code identifier
     * @param $lang snippet lang
     * @return mysqli_result all versions history
     */
    public function loadOtherVersion($id, $lang)
    {
        $queryBuilder = $this->newQueryBuilder();
        $queryBuilder
            ->select('*')
            ->from('Users','u')
            ->join('u','Sources','s',
                $queryBuilder->expr()->eq('u.IDU','s.IDU'))
            ->join('s','Codes','c',
                $queryBuilder->expr()->eq('s.IDC','c.IDC'))
            ->where($queryBuilder->expr()->andX(
                $queryBuilder->expr()->eq('c.IDC','?'),
                $queryBuilder->expr()->eq('s.Lang','?')
            ))
            ->orderBy('Version','ASC')
            ->setParameter(0,$id)
            ->setParameter(1,$lang);
        return $this->execute($queryBuilder);
    }

    /**
     * Load all snippets from a code with different lang.
     *
     * @param $id code identifier
     * @param $lang snippet lang
     * @return mysqli_result all snippet with different lang
     */
    public function loadDiff($id, $lang)
    {
        $queryBuilder = $this->newQueryBuilder();
        $subQueryBuilder = $this->newQueryBuilder();
        $queryBuilder
            ->select('s.Lang','s.Code','s.Version')
            ->from('Sources','s')
            ->where($queryBuilder->expr()->andX(
               $queryBuilder->expr()->neq('s.Lang','?'),
               $queryBuilder->expr()->eq('s.IDC','?'),
               $queryBuilder->expr()->eq('s.Version','('.
                   $subQueryBuilder
                        ->select('MAX(su.Version)')
                        ->from('Sources','su')
                        ->where($subQueryBuilder->expr()->andX(
                            $queryBuilder->expr()->eq('su.IDC','?'),
                            $queryBuilder->expr()->eq('su.Lang','s.Lang')
                        ))->getSQL().')'
               )
            ))
            ->setParameters(array($lang,$id,$id));
        return $this->getData($queryBuilder);
    }

    /**
     * Loads all langs in which a solution is implemented.
     *
     * @param $IDC solution identifier
     * @return array all langs
     */
    public function loadLangs($IDC)
    {
        $queryBuilder = $this->newQueryBuilder();
        $queryBuilder
            ->select('DISTINCT Lang')
            ->from('Sources')
            ->where($queryBuilder->expr()->eq('IDC','?'))
            ->setParameter(0,$IDC);
        $query = $this->getData($queryBuilder);
        $toReturn = array();
        foreach ($query as $var) {
            array_push($toReturn, $var["Lang"]);
        }
        return $toReturn;
    }

    /**
     * Get last version of a snippet.
     *
     * @param $IDC code identifier
     * @param $lang snippet lang
     * @return int the last snippet's version
     */
    public function getLastVersion($IDC, $lang)
    {
        $queryBuilder = $this->newQueryBuilder();
        $queryBuilder
            ->select('MAX(Version)')
            ->from('Sources')
            ->where($queryBuilder->expr()->andX(
                $queryBuilder->expr()->eq('IDC','?'),
                $queryBuilder->expr()->eq('Lang','?')
            ))
            ->setParameter(0,$IDC)
            ->setParameter(1,$lang);
        return $this->getData($queryBuilder)[0]['MAX(Version)'];
    }

    /**
     * Get the last IDC put.
     *
     * @return int the last code identifier
     */
    public function getLastIDC($name)
    {
        $queryBuiler = $this->newQueryBuilder();
        $queryBuiler
            ->select('MAX(IDC)')
            ->from('Codes')
            ->where($queryBuiler->expr()->eq('name','?'))
            ->setParameter(0,$name);
        return $this->getData($queryBuiler)[0]['MAX(IDC)'];
    }

    /**
     * Load all data from code.
     *
     * @param $idc code identifier
     * @return array all code data
     */
    public function loadCode($idc)
    {
        $queryBuilder = $this->newQueryBuilder();
        $queryBuilder
            ->select('*')
            ->from('Codes')
            ->where($queryBuilder->expr()->eq('IDC','?'))
            ->setParameter(0,$idc);
        return $this->getData($queryBuilder);
    }

    /**
     * Load 10 codes according the user's filter
     *
     * @param $array Array whit all  (Post mensage)
     * @return mysqli_result ten codes
     */
    public function loadFilter($array,$global_search)
    {
        $queryBuilder = $this->newQueryBuilder();
        $queryBuilder
            ->select('c.IDC','c.Name','u.nick','s.Lang','c.Description','s.Code','s.Version')
            ->from('Users','u')
            ->join('u','Sources','s',
                $queryBuilder->expr()->eq('u.IDU','s.IDU'))
            ->join('s','Codes','c',
                $queryBuilder->expr()->eq('s.IDC','c.IDC'));

        if($global_search){
            $queryBuilder
                ->where('TRUE');
        }
        else{
            $count = false;
            foreach ($array as $key => $value) {
                if ($key != "search" and $key != "o") {
                    if ($count) {
                        $queryBuilder
                            ->orWhere($queryBuilder->expr()->eq('s.Lang',"'$value'"));
                    } else {
                        $queryBuilder->where($queryBuilder->expr()->eq('s.Lang',"'$value'"));
                        $count = !$count;
                    }
                }
            }
        }
        if (array_key_exists("o",$array))
            $first = $array["o"] * 10;
        else
            $first = 0;
        if(array_key_exists("search",$array))
            $queryBuilder = $this->search($queryBuilder,$array["search"]);
        $subQuery = $this->newQueryBuilder();
        $queryBuilder->andWhere(
            $queryBuilder->expr()->eq('s.Version','('.
                $subQuery
                    ->select('MAX(su.Version)')
                    ->from('Sources','su')
                    ->where($subQuery->expr()->andX(
                        $subQuery->expr()->eq('s.Lang','su.Lang'),
                        $subQuery->expr()->eq('s.IDC','su.IDC'))
                    )->getSQL().')'
            ))
            ->orderBy('Modification','DESC')
            ->setFirstResult($first)
            ->setMaxResults(10);
        return $this->execute($queryBuilder);
    }

    /**
     * make a fragment of a query based in the text which user inputs
     *
     * @param $text input by user
     * @return string sql query fragment
     */
    public function search(Doctrine\DBAL\Query\QueryBuilder $queryBuilder, $text)
    {
        $subWhere = "";
        if ($text != "") {
            $textExplode = explode(" ", $text);
            $value = 0;
            foreach ($textExplode as $find) {
                if ($find != "") {
                    if ($value != 0) {
                        $subWhere .= " AND ";
                    }
                    $subWhere.="c.Description LIKE ".$queryBuilder->getConnection()->quote("%$find%")." OR c.Name LIKE ".$queryBuilder->getConnection()->quote("%$find%");
                    $value++;
                }
            }
            return $queryBuilder->andWhere($subWhere);
        } else {
            return $queryBuilder;
        }
    }

    //SQL INSERT/UPDATE

    /**
     * Add a source of a code.
     *
     * @param $IDC code identifier
     * @param $lang snippet's lang
     * @param $code the source code
     * @param $IDU User identifier
     * @param null $extlib external library
     * @param null $extlibver external library version
     * @return int return the snippet's version
     */
    public function addSource($IDC, $lang, $code, $IDU, $extlib = null, $extlibver = null)
    {
        $lib  = $extlib;
        $libV = $extlibver;
        if($lib == null) {
            $lib = "";
            $libV = "";
        }

        $save = false;
        $version = $this->getLastVersion($IDC, $lang);
        if ($version == null){
            $save = true;
            $version = 0;
        }
        $version++;
        $modification = time();
        $queryBuilder = $this->newQueryBuilder();
        $queryBuilder
            ->insert('Sources')
            ->values(
                array(
                    'IDC'           => '?',
                    'Lang'          => '?',
                    'Version'       => '?',
                    'Modification'  => '?',
                    'Code'          => '?',
                    'UseExtLib'     => '?',
                    'UseExtLibVer'  => '?',
                    'IDU'           => '?'
                )
            )
            ->setParameters(array($IDC,$lang,$version,$modification,$code,$lib,$libV,$IDU));
        $this->execute($queryBuilder);
        if($save){
            $this->save($IDU, $IDC, $lang);
        }
        return $version;
    }

    /**
     * Add or modify a code in database.
     *
     * @param $IDC code identifier (0 if new)
     * @param $name solutions's name
     * @param $description solution's description
     * @param $input solution's input example
     * @param $output solution's output example
     * @param $lang source's lang
     * @param $code source's code
     * @param $IDU user identifier
     * @param null $extlib external library
     * @param null $extlibver external library version
     * @return int the snippet's version or IDC
     */
    public function addOrModifyCodes($IDC, $name, $description, $input, $output, $lang, $code, $IDU, $extlib = null, $extlibver = null)
    {
        if ($IDC == 0) {
            $queryBuilder = $this->newQueryBuilder();
            $queryBuilder
                ->insert('Codes')
                ->values(array(
                    'UserCreator'   => '?',
                    'Name'          => '?',
                    'Description'   => '?',
                    'Input'         => '?',
                    'Output'        => '?'
                ))
                ->setParameters(array($IDU,$name,$description,$input,$output));
            //$query = "INSERT INTO Codes (`UserCreator`,`Name`,`Description`,`Input`,`Output`) VALUES ('$IDU','$name','$description','$in','$out') ";
            $this->execute($queryBuilder);
            $myID = $this->getLastIDC($name);
            $this->addSource($myID, $lang, $code, $IDU, $extlib, $extlibver);
            return $myID;
        } else {
            $arr = $this->loadAll($IDC, $lang, $this->getLastVersion($IDC, $lang));
            $codewrite = $arr["Code"];
            $version = 0;
            if ($code != $codewrite) {
                $version = $this->addSource($IDC, $lang, $code, $IDU, $extlib, $extlibver);
            } else if ($extlib != $arr["UseExtLib"] || $extlibver != $arr["UseExtLibVer"]) {

                $queryBuilder = $this->newQueryBuilder();
                $queryBuilder
                    ->update('Sources')
                    ->set('UseExtLib','?')
                    ->set('UseExtLibVer','?')
                    ->where($queryBuilder->expr()->andX(
                        $queryBuilder->expr()->eq('IDC','?'),
                        $queryBuilder->expr()->eq('Lang','?'),
                        $queryBuilder->expr()->eq('Version','?')
                    ))
                    ->setParameter(0,$extlib)
                    ->setParameter(1,$extlibver)
                    ->setParameter(2,$IDC)
                    ->setParameter(3,$arr['Lang'])
                    ->setParameter(4,$arr['Version']);
                $this->execute($queryBuilder);
            }
            $queryBuilder = $this->newQueryBuilder();
            $queryBuilder
                ->update('Codes')
                ->set('Name','?')
                ->set('Description','?')
                ->set('Input','?')
                ->set('Output','?')
                ->where($queryBuilder->expr()->eq('IDC','?'))
                ->setParameters(array($name,$description,$input,$output,$IDC));
            $this->execute($queryBuilder);
            return $version;
        }
    }

    //SQL DELETE

    /*public function deleteCode($idc){
        $query = "DELETE FROM SCALE WHERE IDC=$idc";
        dbw_query($this->conn,$query);
        $query = "DELETE FROM Codes WHERE IDC=$idc";
        dbw_query($this->conn,$query);
    }*/

    /*public function deleteSource($idc,$lang,$ver){
        $query = "DELETE FROM Sources WHERE IDC=$idc AND Lang='$lang' AND Version=$ver";
        dbw_query($this->conn,$query);
    }*/

    /*
     * Users
     */

    //SQL SELECT

    /**
     * Load user profile
     *
     * @param $id user identifier
     * @return array all user profile
     */
    public function loadProfile($id)
    {
        $queryBuilder = $this->newQueryBuilder();
        $queryBuilder
            ->select('*')
            ->from('Users','u')
            ->where($queryBuilder->expr()->eq('u.IDU','?'))
            ->setParameter(0,$id);
        return $this->getData($queryBuilder)[0];
    }

    /**
     * Load user IDU from $email
     *
     * @param $email user's email
     * @return int user's identifier
     */
    public function loadIDU($field,$mode='email')
    {
        $queryBuilder = $this->newQueryBuilder();
        $queryBuilder
            ->select('IDU')
            ->from('Users')
            ->where($queryBuilder->expr()->eq($mode,'?'))
            ->setParameter(0,$field);
        return $this->getData($queryBuilder)[0]['IDU'];
    }

    /**
     * Check if user cookie token is the same saved in database.
     *
     * @param $IDU user's identifier
     * @param $token token in cookie
     * @return bool true if same, false if not
     */
    public function checkCookie($IDU, $token)
    {
        $queryBuilder = $this->newQueryBuilder();
        $queryBuilder
            ->select('token')
            ->from('Users')
            ->where($queryBuilder->expr()->eq('IDU','?'))
            ->setParameter(0,$IDU);
        $tokenDB = $this->getData($queryBuilder)[0]["token"];
        if ($tokenDB == $token)
            return true;
        else
            return false;
    }

    /**
     * Check if password is correct.
     *
     * @param $email user email
     * @param $pass pass password input
     * @return bool true if match, false if not
     */
    public function checkPass($email, $pass)
    {
        $queryBuilder = $this->newQueryBuilder();
        $queryBuilder
            ->select("pass")
            ->from("Users")
            ->where($queryBuilder->expr()->eq('email','?'))
            ->setParameter(0,$email);
        $passDB = $this->getData($queryBuilder)[0]["pass"];
        if ($passDB == hash('sha256', $pass))
            return true;
        else
            return false;
    }

    //SQL INSERT/UPDATE

    /**
     * Register a new user in platform.
     *
     * @param $email user's email
     * @param $pass user's password
     * @param $nick user's nickname
     * @return bool true if do not exist the email, false if it exists
     */
    public function register($email, $pass, $nick)
    {
        $token = RandomString(50);
        if ($this->loadIDU($email))
            return 'MAIL_IN_USE';
        else if ($this->loadIDU($nick,'nick'))
            return 'NICK_IN_USE';
        else {
            $password = hash('sha256', $pass);
            $queryBuilder = $this->newQueryBuilder();
            $queryBuilder
                ->insert('Users')
                ->values(array(
                    'email' => '?',
                    'pass'  => '?',
                    'nick'  => '?',
                    'token' => '?'
                ))
                ->setParameters(array($email,$password,$nick,hash('sha256',$token)));
            $this->execute($queryBuilder);
            newUser($email, $nick, $token);
            return 'CORRECT';
        }
    }

    /**
     * Set token
     *
     * @param $IDU user's identifier
     * @param $token autogenerated token
     */
    public function setToken($IDU, $token)
    {
        $queryBuilder = $this->newQueryBuilder();
        $queryBuilder
            ->update('Users')
            ->set('token','?')
            ->where($queryBuilder->expr()->eq('IDU','?'))
            ->setParameter(0,$token)
            ->setParameter(1,$IDU);
        $this->execute($queryBuilder);
    }

    /**
     * Set role
     *
     * @param $IDU user's identifier
     * @param $role Role
     */
    public function setRole($IDU, $role=1)
    {
        $queryBuilder = $this->newQueryBuilder();
        $queryBuilder
            ->update('Users')
            ->set('ROLE','?')
            ->where($queryBuilder->expr()->eq('IDU','?'))
            ->setParameter(0,$role)
            ->setParameter(1,$IDU);
        $this->execute($queryBuilder);
    }

    /**
     * Change password
     *
     * @param $idu user identifier
     * @param $pass new password to update
     */
    public function updatePass($idu, $pass)
    {
        $queryBuilder = $this->newQueryBuilder();
        $queryBuilder
            ->update('Users')
            ->set('pass','?')
            ->where($queryBuilder->expr()->eq('IDU','?'))
            ->setParameter(0,hash('sha256',$pass))
            ->setParameter(1,$idu);
        $this->execute($queryBuilder);
    }

    /**
     * Check if token to restore password is correct
     * 
     * @param $nick string $nick string nick to restore password
     * @param $token string $token  random string generate
     * @param $timestamp int $timestamp  moment when restore activate
     * @return bool true is are similar
     */
    public function checkRestoreToken($nick,$token,$timestamp){
        $queryBuilder = $this->newQueryBuilder();
        $queryBuilder
            ->select('token')
            ->from('Users')
            ->where($queryBuilder->expr()->eq('nick','?'))
            ->setParameter(0,$nick);
        $tk = $this->getData($queryBuilder)[0]['token'];
        return $tk == hash('sha256',"$token-$timestamp");
    }

    /**
     * Check if token to confirm is correct
     *
     * @param $token string $token  random string generate
     * @param $idu int user identifier
     * @return bool true is are similar
     */
    public function checkConfirmToken($idu,$token){
        $queryBuilder = $this->newQueryBuilder();
        $queryBuilder
            ->select('token')
            ->from('Users')
            ->where($queryBuilder->expr()->eq('idu','?'))
            ->setParameter(0,$idu);
        $tk = $this->getData($queryBuilder)[0]['token'];
        return $tk == hash('sha256',"$token");
    }

    /**
     * Generate a random token to restore password
     *
     * @param $email email from user to restore
     * @return string token with timestamp
     */
    public function createRestoreToken($email){
        $token = randomString(75);
        $now = time();
        $queryBuilder = $this->newQueryBuilder();
        $queryBuilder
            ->update('Users')
            ->set('token','?')
            ->where($queryBuilder->expr()->eq('email','?'))
            ->setParameter(0,hash('sha256',"$token-$now"))
            ->setParameter(1,$email);
        $this->execute($queryBuilder);
        return "$token-$now";
    }

    /* Save and like */

    /**
     * Vote or save a code by user
     *
     * @param $idu user identifier
     * @param $idc code identifier
     * @param $lang lang of source
     * @param $table table, Votes or Save
     * @return int 0 if correct, 1 if error
     */
    private function voteOrSave($idu,$idc,$lang,$table){
        $queryBuilder = $this->newQueryBuilder();
        $queryBuilder
            ->insert($table)
            ->values(
                array(
                    "IDU" => '?',
                    "IDC" => '?',
                    "Lang"=> '?'
                )
            )
            ->setParameters(array($idu,$idc,$lang));

        try{
            $this->execute($queryBuilder);
            return 0;
        }catch(Exception $ex){
            return 1;
        }
    }

    /**
     * Vote a code source
     *
     * @param $idu user identifier
     * @param $idc code identifier
     * @param $lang source's lang
     * @return int 0 if correct, 1 if error
     */
    public function vote($idu,$idc,$lang){
        return $this->voteOrSave($idu,$idc,$lang,"Likes");
    }

    /**
     * Save a code source
     *
     * @param $idu user identifier
     * @param $idc code identifier
     * @param $lang source's lang
     * @return int 0 if correct, 1 if error
     */
    public function save($idu,$idc,$lang){
        return $this->voteOrSave($idu,$idc,$lang,"Saves");
    }

    /**
     * Remove a vote or save
     *
     * @param $idu user identifier
     * @param $idc code identifier
     * @param $lang lang of source
     * @param $table table, Votes or Saves
     * @return int 0 if correct, 1 if error
     */
    private function unvoteOrUnsave($idu,$idc,$lang,$table){
        $queryBuilder = $this->newQueryBuilder();
        $queryBuilder
            ->delete($table)
            ->where($queryBuilder->expr()->andX(
                $queryBuilder->expr()->eq('IDU','?'),
                $queryBuilder->expr()->eq('IDC','?'),
                $queryBuilder->expr()->eq('Lang','?')
            ))
            ->setParameters(array($idu,$idc,$lang));
        try{
            $this->execute($queryBuilder);
            return 0;
        }catch(Exception $ex){
            return 1;
        }
    }

    /**
     * Unvote a code source
     *
     * @param $idu user identifier
     * @param $idc code identifier
     * @param $lang source's lang
     * @return int 0 if correct, 1 if error
     */
    public function unvote($idu,$idc,$lang){
        return $this->unvoteOrUnsave($idu,$idc,$lang,"Likes");
    }

    /**
     * Unsave a code source
     *
     * @param $idu user identifier
     * @param $idc code identifier
     * @param $lang source's lang
     * @return int 0 if correct, 1 if error
     */
    public function unsave($idu,$idc,$lang){
        return $this->unvoteOrUnsave($idu,$idc,$lang,"Saves");
    }

    /**
     * Get all sources saved by user
     *
     * @param $idu user identifier
     * @return array IDC, Lang and Version
     */
    public function allSaves($idu){
        $queryBuilder = $this->newQueryBuilder();
        $subQuery = $this->newQueryBuilder();
        $queryBuilder
            ->select(array('s.IDC','s.Lang','s.Version','c.Name'))
            ->from('Saves','sa')
            ->join('sa','Sources','s',
                $queryBuilder->expr()->andX(
                    $queryBuilder->expr()->eq("sa.IDC","s.IDC"),
                    $queryBuilder->expr()->eq("sa.Lang","s.Lang")
                ))
            ->join('s','Codes','c',
                $queryBuilder->expr()->eq("s.IDC",'c.IDC'))
            ->where(
                $queryBuilder->expr()->andX(
                    $queryBuilder->expr()->eq("sa.IDU",'?'),
                    $queryBuilder->expr()->eq(
                        's.Version','('.
                        $subQuery
                            ->select('MAX(Version)')
                            ->from('Sources')
                            ->where(
                                $subQuery->expr()->andX(
                                    $subQuery->expr()->eq('s.Lang','Lang'),
                                    $subQuery->expr()->eq('s.IDC','IDC')
                                ))
                            ->getSql()
                        .')')
                )
            )
            ->setParameter(0,$idu);
        return $this->getData($queryBuilder);
    }

    /**
     * Count how many saved sources have an user
     *
     * @param $idu user identifier
     * @return int how many saves
     */
    public function allSavesCount($idu){
        $queryBuilder = $this->newQueryBuilder();
        $queryBuilder
            ->select('COUNT(*)')
            ->from('Saves')
            ->where($queryBuilder->expr()->eq("IDU",'?'))
            ->setParameter(0,$idu);
        return $this->getData($queryBuilder)[0]['MAX(*)'];
    }

    /**
     * Know if user votes or saves a source
     *
     * @param $idu user identifier
     * @param $idc code identifier
     * @param $lang source's lang
     * @param $table Votes or Saves
     * @return int 0 or 1
     */
    private function iVoteSave($idu,$idc,$lang,$table){
        $queryBuilder = $this->newQueryBuilder();
        $queryBuilder
            ->select('COUNT(IDU)')
            ->from($table)
            ->where($queryBuilder->expr()->andX(
                $queryBuilder->expr()->eq('IDU','?'),
                $queryBuilder->expr()->eq('IDC','?'),
                $queryBuilder->expr()->eq('Lang','?')
            ))
            ->setParameters(array($idu,$idc,$lang));
        return $this->getData($queryBuilder)[0]['COUNT(IDU)'];
    }

    /**
     * Know if user votes or saves a source
     *
     * @param $idu user identifier
     * @param $idc code identifier
     * @param $lang source's lang
     * @return int 0 or 1
     */
    public function iVote($idu,$idc,$lang){
        return $this->iVoteSave($idu,$idc,$lang,"Likes");
    }

    /**
     * Know if user votes saves a source
     *
     * @param $idu user identifier
     * @param $idc code identifier
     * @param $lang source's lang
     * @return int 0 or 1
     */
    public function iSave($idu,$idc,$lang){
        return $this->iVoteSave($idu,$idc,$lang,"Saves");
    }

    /**
     * How many votes have a source
     *
     * @param $idc code identifier
     * @param $lang source's lang
     * @return int number of votes
     */
    public function sourceLikes($idc,$lang){
        $queryBuilder = $this->newQueryBuilder();
        $queryBuilder
            ->select('COUNT(IDU)')
            ->from('Votes')
            ->where($queryBuilder->expr()->andX(
                $queryBuilder->expr()->eq("IDC",'?'),
                $queryBuilder->expr()->eq("Lang",'?')
            ))
            ->setParameters(array($idc,$lang));

        return $this->getData($queryBuilder)[0]['COUNT(IDU)'];
    }
}