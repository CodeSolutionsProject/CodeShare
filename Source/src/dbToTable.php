<?php
/**
 * Created by PhpStorm.
 * User: joselucross
 * Date: 7/07/17
 * Time: 11:42
 */


/*
 * Methods to get an array to send to twig
 */

function lastToArray(&$query,$supported,$db,$idu=0){
    $last = array();
    $list = $query->fetchAll();
    foreach($list as $var){
        $like=0;
        $save=0;
        if($idu!=0){
            $like = $db->iVote($idu,$var['IDC'],$var['Lang']);
            $save = $db->iSave($idu,$var['IDC'],$var['Lang']);
        }
        $array = array
        (   "idc" => $var["IDC"],
            "lang" => $var["Lang"],
            "version" => $var["Version"],
            "name" => $var["Name"],
            "nick" => $var["nick"],
            "lLang" => $supported[$var["Lang"]][0],
            "description" => $var["Description"],
            "code" => html_entity_decode($var["Code"]),
            "like" => $like,
            "save" => $save
        );
        array_push($last,$array);
    }
    return $last;
}

function allCodeToArray(&$query,$supported,$db,$idu=0){
    $like=0;
    $save=0;
    if($idu!=0){
        $like = $db->iVote($idu,$query['IDC'],$query['Lang']);
        $save = $db->iSave($idu,$query['IDC'],$query['Lang']);
    }
    $code = array(
        "idc" => $query['IDC'],
        "lang" => $query['Lang'],
        "version" => $query['Version'],
        "name" => $query['Name'],
        "nick" => $query['nick'],
        "idu" => $query['IDU'],
        "lLang" => $supported[$query['Lang']][0],
        "description" => $query['Description'],
        "code" => html_entity_decode($query['Code']),
        "input" => html_entity_decode($query['Input']),
        "output" => html_entity_decode($query['Output']),
        "rows"  =>  substr_count($query['Code'],"\n"),
        "extlib" => $query['UseExtLib'],
        "extlibver" => $query['UseExtLibVer'],
        "like" => $like,
        "save" => $save
    );
    return $code;

}

function otherImplementationToArray(&$query,$supported){
    $other = array();
    foreach($query as $var){
        $array = array
        (   "lang" => $var["Lang"],
            "version" => $var["Version"],
            "lLang" => $supported[$var["Lang"]][0],
            "code" => html_entity_decode($var["Code"]),
        );
        array_push($other,$array);
    }
    return $other;
}

function savedToArray($query,$supported){
    $saved = array();
    foreach($query as $var){
        $array = array
        (
            "idc" => $var['IDC'],
            "lang" => $var['Lang'],
            "version" => $var['Version'],
            "lLang" => $supported[$var["Lang"]][0],
            "name" => $var['Name']
        );
        array_push($saved,$array);
    }
    return $saved;
}

function otherVersionToArray(&$query,$supported,$db,$idu){
    return lastToArray($query,$supported,$db,$idu);
}