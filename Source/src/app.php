<?php
if(isset($_COOKIE['cookieconsent_status']) and $_COOKIE['cookieconsent_status']!="deny") {
    session_start(); //Start session in app, before loading vars ($_SESSION)
}

require_once __DIR__ .'/../vendor/autoload.php';

require_once __DIR__ . '/Config.php';
require 'functions.php';
require 'domails.php';
require 'DB.php';
require 'dbToTable.php';
require 'users.php';



$app = new Silex\Application();

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

$app['debug'] = false;
$app->register(new Silex\Provider\UrlGeneratorServiceProvider());
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/../',
));


$app['url'] = '/';

$app['front'] = $app['url'] . 'assets';
$valores = array('js', 'img', 'css', 'fonts', 'html');
foreach($valores as $asset){
    $app['front'.$asset] = $app['front'].'/'.$asset;
}

$string = file_get_contents(__DIR__."/../data/supported.json");
$app["supported"] = json_decode($string, true);

$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'db.options' => Config::getDBalConexion()
));

$app['data'] = new DB($app['db']);

/* Security */
$app['security.role_hierarchy'] = array(
	'ROLE_DELETE' => array('ROLE_COMMON'),
	'ROLE_COMMON' => array('ROLE_USER'),
);
/* End Security */

$app->boot();

/* Basics */
$app->get('/', function (Request $request) use($app) {

    $user = getUser($app);
    $idu=0;
    if($user != null){
        $idu=$app['data']->loadIDU($user['nick'],'nick');
    }

    $ajax=false;
    if($request->getMethod()=="POST"){
        if($request->get('globalSearch')!=null){
            $data = array('search'=>$request->get('globalSearch'));
            $type=true;
        }else{
            $data = $request->request->all();
            $type=false;
            $ajax=true;
        }
        $last = $app['data']->loadFilter($data,$type);
        $last = lastToArray($last,$app['supported'],$app['data'],$idu);
    }else{
        $query = $app['data']->loadLast();
        $last = lastToArray($query,$app['supported'],$app['data'],$idu);
    }
    $page = array(  'title'       => "CodeShare", 
                    "description" => "Sharing your solutions with all",
                    "last"        => $last,
                    "supported"   => $app["supported"],
                    "page"        => "home",
                    "grouped"     => groupByCategory($app["supported"]));
    if($ajax){
        return $app['twig']->render($app['fronthtml'] . '/firstCodes.twig' ,array(
            "page" => $page,
            'user' => $user
        ));
    }else{
        return $app['twig']->render($app['fronthtml'] . '/firstpage.twig' ,array(
            "page" => $page,
            'user' => $user
        ));
    }
})->bind('home')->method('GET|POST');


$app->get('/doc', function (Request $request) use($app){

    $user = getUser($app);
    $page = array(
        "title"     =>  "Documentation",
        "description" => "All documentation about how to upload code in CodeShare",
        "page"      =>   "doc",
    );
    return $app['twig']->render($app['fronthtml'].'/doc.twig', array("page" => $page, "user" => $user));
})->bind('doc');

$app->get('/about', function (Request $request) use($app){

    $user = getUser($app);
    $page = array(
        "title"     =>  "About CodeShare",
        "description" => "Developed by Code Solutions Project's Team",
        "page"      =>  "about",
    );
    return $app['twig']->render($app['fronthtml'].'/about.twig', array("page" => $page,'user'=>$user));
})->bind('about');

/* User */
$app->get('/tos',function(Request $request) use($app){

    $user = getUser($app);
    $page = array(
        "title"     => "Terms of service",
        "description" => "Terms of services and privacy policies",
        "page" => ""
    );

    return $app['twig']->render($app['fronthtml'].'/TOS.twig', array("page"  =>  $page, "user" => $user));
})->bind('tos');    

$app->get('/login', function(Request $request) use ($app){

    $user = getUser($app);
    if($user != null){
        return $app->redirect($app["url_generator"]->generate('home'));
    }
    if ($request->getMethod() == 'GET'){
        return loginRegister('login');
    }
    else{
        $state = checkInfo($request);
        return loginOrRegister($state,$request);
    }

})->bind('login')->method('GET|POST');

$app->get('/signup', function(Request $request) use ($app){

    $user = getUser($app);
    if($user != null){
        return $app->redirect($app["url_generator"]->generate('home'));
    }

    if ($request->getMethod() == 'GET'){
        return loginRegister('sign-up');
    }else{
        $state = checkInfo($request);
        return loginOrRegister($state,$request);
    }

})->bind('register')->method('GET|POST');

function loginRegister($default,$state=''){
    global $app;
    if($state!=''){
        return $state;
    }
    $page = array(
        "title"     =>  ucfirst($default),
        "description" => "Start now in CodeShare",
        "page"      =>  "$default",
        "state"     =>  $state, //0=no logged, 1 login fail, 2 register fail, 3 all correct (render home)
    );
    return $app['twig']->render($app['fronthtml']."/loginregister.twig", array("page" => $page));
}

function loginOrRegister($state,$request){
    global $app;
    switch ($state){
        case 3:
            return loginRegister('login','BAD_CREDENTIAL');
        case 1:
            $result = checklogin($request->get('email'),$request->get('pwd'),$app['data']);
            if($result==''){
                return 'redirect';
            }else if($result=='VALIDATE'){
                return loginRegister('login','CONFIRM_MAIL');
            }else{
                return loginRegister("login",'BAD_CREDENTIAL');
            }
        case 2:
            $captcha = checkCaptcha($request->get("g-recaptcha-response"));
            if(!$captcha){
                return "CAPTCHA_FAIL";
            }

            $state = register($request->get('emailre'),$request->get('emailre-re'),
            $request->get('pwdre'),  $request->get('pwdre-re'),$request->get('nick'),$app['data']);
            if ($state == '') 
                return 'redirect'; 
            else
                return loginRegister("sing-up",$state);
            
    }
}

$app->get('logout',function(Request $request) use ($app){
    logout();
    return $app->redirect($app['url_generator']->generate('home'));
})->bind('logout');

$app->get('/user', function(Request $request) use ($app){

    $user = getUser($app);
    if($user == null)
        $app->abort('403');

    $status = 0;
    if($request->getMethod() == 'POST'){
        $passact = $request->get("passact");
        $newpass = $request->get("newpass");
        $newpassre = $request->get("newpass-re");
        if($app['data']->checkPass($user['email'],$passact) && $newpass == $newpassre){
            $status=1;
            $app['data']->updatePass($user['IDU'],$newpass);
        }else
            $status=2;
    }
    $page = array(
        "page" => "user",
        "title" => "$user[nick]",
        "description" => "NOT RELEVANT",
        "status" => $status
    );
    $saved = $app['data']->allSaves($app['data']->loadIDU($user['nick'],'nick'));
    return $app['twig']->render($app['fronthtml']."/user.twig",array("page"=>$page,"user"=>$user,"saved"=>savedToArray($saved,$app['supported'])));
    
})->bind('user')->method('GET|POST');

/* Error Codes */
function HTTPError($code){
    switch($code){
        case 400:
            $text = "Bad Request";
            break;
        case 401:
            $text = "Unauthorized user";
            break;
        case 403:
            $text = "Forbidden page";
            break;
        case 404:
            $text = "Page not found";
            break;
        case 408:
            $text = "Time out";
            break;
        case 503:
            $text = "Database unavailable";
            break;
        case 500:
            $text = "Internal server error";
            break;
        default:
            $text = "Please, return to home page";
            $code = "Internal error";
            break;
    }
    return array("text"=>$text,"number"=>$code);
}

$app->error(function (\Exception $e,$request) use ($app) {
    $user = null;
    try{
        $user = getUser($app);
    }catch(Throwable $ex){
        //Nothing
    }
    if ($app['debug']) {
        return;
    }else {
        $code=500;
        if($e instanceof \Symfony\Component\HttpKernel\Exception\HttpException)
            $error = HTTPError($e->getStatusCode());

        
    }
    return $app['twig']->render($app['fronthtml'].'/error.twig', Array(
        'page' => array("title"=>$code),
        'error' => $error,
        'user' => $user,
    ));
});


/* Codes */
$app->get('/code/{lang}-{idc}-{version}', function (Request $request, $lang, $idc, $version) use($app){

    $user = getUser($app);

    $array = $app['data']->loadAll($idc,$lang,$version);
    if(!$array){
        $app->abort('404');
    }
    if($request->getMethod()=='POST'){
        return updateCode($request,$idc,$lang,$version,$user);
    }
    $page = array(
        "page"          => 'code',       
        "title"         =>  "$array[Name] by $array[nick]",
        "description"   => "$array[Name] by $array[nick] in ".$app['supported'][$lang][0],
        "otherV"        =>  false,
        "otherI"        =>  false,
        "existedLangs"  =>  $app['data']->loadLangs($idc),
        "original"      =>  $app['data']->loadOriginalAuthor($idc),
    );
    $idu=0;
    if($user != null){
        $idu=$app['data']->loadIDU($user['nick'],'nick');
    }
    $code = allCodeToArray($array,$app['supported'],$app['data'],$idu);
    if($array['Version']!=1)
        $page['otherV'] = true;
    $diff = $app['data']->loadDiff($idc,$lang);
    if(count($diff)>=1){
        $page['otherI'] = true;
    }
    $otherImplementation = otherImplementationToArray($diff,$app['supported']);
    return $app['twig']->render($app['fronthtml'].'/code.twig', array(
        "page"                  =>  $page,
        "code"                  =>  $code,
        "otherImplementation"   =>  $otherImplementation,
        "supported"             =>  $app['supported'],
        "user"                  =>  $user
    ));
})->bind('code')->method('GET|POST');

function updateCode(Request $request,$idc,$lang,$version,$user){
    global $app;

    $lang2 = $request->request->all()['lang'];
    $extlib = $request->get('extlib');
    $extlibver = $request->get('extlibver');
    if($extlibver == null and $extlib != null)
        $app->abort(400);
    $code = htmlentities($request->get('code'));
    if ($code == null or $lang == null)
        $app->abort(400);
    if($lang2 != null){
        $app['data']->addSource($idc,$lang2,$code,$user['IDU'],$extlib,$extlibver);
        return $app->redirect($app['url_generator']->generate('code',array("idc"=>$idc,"version"=>1,"lang"=>$lang2)));
    }else{
        $name = $request->get("name");
        $description = $request->get("description");
        $input = htmlentities($request->get("input"));
        $output = htmlentities($request->get("output"));
        if($name == null || $description == null || $input == null ||$output == null)
            $app->abort(400);
        $version2 = $app['data']->addOrModifyCodes($idc,$name,$description,$input,$output,$lang,$code,$user['IDU'],$extlibver,$extlibver);
        if($version2!=0)
            return $app->redirect($app['url_generator']->generate('code',array("idc"=>$idc,"version"=>$version2,"lang"=>$lang)));
        else
            return $app->redirect($app['url_generator']->generate('code',array("idc"=>$idc,"version"=>$version,"lang"=>$lang)));
    }
}

$app->get('/code/{lang}-{idc}', function(Request $request, $lang, $idc) use($app){

    $user = getUser($app);
    $idu=0;
    if($user != null){
        $idu=$app['data']->loadIDU($user['nick'],'nick');
    }

    $array = $app['data']->loadOtherVersion($idc,$lang);
    $other = otherVersionToArray($array,$app['supported'],$app['data'],$idu);
    $name = $other[0]['name'];

    $page = array(
        "title" =>  "Other versions of $name",
        "description" => "All differents version of $name uploaded in CodeShare",
        "last"  =>  $other,
        "page"  =>  ""
    );
    return $app['twig']->render($app['fronthtml'].'/otherVersion.twig', array(
        "page"  =>  $page, "user" => $user
    ));
})->bind('codeVer');

$app->get('/add', function(Request $request) use($app){

    $user = getUser($app);
    if ($user == null)
        return $app->redirect($app['url_generator']->generate('login'));
    if($request->getMethod()=="POST"){
        $extlib = $request->get('extlib');
        $extlibver = $request->get('extlibver');
        if($extlib == null or $extlibver != null){
            $name = $request->get('name');
            $description = $request->get('description');
            $input = htmlentities($request->get('input'));
            $output = htmlentities($request->get('output'));
            $lang = $request->get('lang');
            $code = htmlentities($request->get('code'));
            if($name == null || $description == null || $input == null ||
            $output == null || $lang == null || $code == null){
                $app->abort(400);
            }
            else{
                $idc = $app['data']->addOrModifyCodes(
                    0,
                    $name,
                    $description,
                    $input,
                    $output,
                    $lang,
                    $code,
                    $user['IDU'],
                    $extlib,
                    $extlibver
                );
                return $app->redirect($app['url_generator']->generate('code',array(
                     "lang"=>$lang,"idc"=>$idc, "version"=>1
                )));
            }
        }else{
            $app->abort(400);
        }
    }else{
        $page=array(
            "title" =>  "Add your own solution",
            "description" => "Share now your snippet",
            "page"  =>  "add",
        );
        return $app['twig']->render($app['fronthtml']."/add.twig", array("page" => $page, "user"=>$user, "supported"   => $app["supported"]));
    }
    
})->bind('add')->method('GET|POST');

$app->get('/restore', function(Request $request) use($app){

    if($request->getMethod()=="POST"){
        $parts = $app['data']->createRestoreToken($request->get('emailre'));
        $parts = explode("-", $parts);
        $token = $parts[0];
        $timestamp = $parts[1];
        restorePassword(
            $request->get('emailre'),
            $app['data']->loadProfile($app['data']->loadIDU($request->get('emailre')))['nick'],
            $token,
            $timestamp
        );
        return "UPDATE";
    }
    $page = array("title"=>"Restore your password",
                  "description"=>"None",
                  "page" =>"None");
    return $app['twig']->render($app['fronthtml']."/restore.twig",array("page" => $page));

})->bind('restore')->method('GET|POST');

$app->get('/restore/{nick}/{token}-{timestamp}', function(Request $request, $nick, $token, $timestamp) use($app){
    $time = 3600; //An hour
    $now = time();
    if($now - $timestamp > $time){
        $app->abort(408);
    }else{
        if(!$app['data']->checkRestoreToken($nick,$token,$timestamp))
            $app->abort(401);
        if($request->getMethod()=='POST'){
            $pass = $request->get('pwdre');
            $passre = $request->get('pwdre-re');
            if($pass != $passre){
                return "NO_MATCH";
            }
            else{
                $idu = $app['data']->loadIDU($request->get('emailre'));
                $iduC = $app['data']->loadIDU($nick,'nick');
                if($idu != $iduC)
                    return "BAD_EMAIL";
                $app['data']->updatePass($idu,$pass);
            }
            $app['data']->setToken($idu,"");
            return "UPDATE";
        }else{
            $page = array(
                "title"         => "Restore your password",
                "description"   => "Page to restore your password",
                "page"          => "NONE",
                "nick"          => $nick,
                "token"         => $token,
                "timestamp"     => $timestamp,
            );
            return $app['twig']->render($app['fronthtml']."/restorePass.twig",array("page" => $page));
        }
    }
})->bind('restorePass')->method('GET|POST');

$app->get('/confirm/{nick}-{token}', function(Request $request, $nick, $token) use($app){
    $idu=$app['data']->loadIDU($nick,'nick');
    $res = $app['data']->checkConfirmToken($idu,$token);
    if($res)
        $app['data']->setRole($idu);
    else
        $app->abort(401);
    return $app->redirect($app['url_generator']->generate('login'));
})->bind('confirm');

/* Votes and saves */

$app->post('/vote-save',function(Request $request) use($app){

    $user = getUser($app);
    if ($user == null)
        return 1;

    $data = str_replace('&quot;','"',$request->getContent());
    $data = json_decode($data,true);
    $idu = $app['data']->loadIDU($user['nick'],'nick');
    switch($data['mode']){
        case "vote":
            return $app['data']->vote($idu,$data['idc'],$data['lang']);
        case "unvote":
            return $app['data']->unvote($idu,$data['idc'],$data['lang']);
        case "save":
            return $app['data']->save($idu,$data['idc'],$data['lang']);
        case "unsave":
            return $app['data']->unsave($idu,$data['idc'],$data['lang']);
    }
    return 1;

})->bind('vote-save');