function doAjax(postValue) {
    //document.getElementById('ajaxPut').innerHTML = "<div class='container-fluid'><div class='row'></div><div class='row'><div class='loader'></div></div></div></div>";
    // Obtener la instancia del objeto XMLHttpRequest (ajax)
	conexion = new XMLHttpRequest();
	// Preparar la funcion de respuesta
	conexion.onreadystatechange = ajaxresponse; //Cuando el ajax sea procesado y suceda algo, se ejecuta esta funcion
	// Realizar peticion HTTP
	conexion.open('POST', '/');
	conexion.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	conexion.send(postValue); //Las variables a mandar, en este caso POST
}
newFilter=false;
function ajaxresponse(){
    if(conexion.readyState == 4){ //Esto es para que cambie cuando haya respuesta, que no es en todos los momentos
        o++;
        var ajaxPut = document.getElementById('ajaxPut');
        if(newFilter){
            newFilter=false;
            ajaxPut.innerHTML = conexion.responseText; //Pondra todo lo devuelto por "file" (Que sera un php que de //echos dependiendo de lo enviado) en un div
        }else{  
            if($("#end").length == 0)
                ajaxPut.innerHTML = ajaxPut.innerHTML+conexion.responseText;
        }
    }
    reHightlight();
}

function reHightlight(){
    var codebox = document.getElementsByClassName("toHightlight");
    for(var i=0;i<codebox.length;++i){
        hljs.highlightBlock(codebox[i]);
    }
    for(var i=0;i<codebox.length;++i){
        codebox[i].classList.remove('toHightlight');
    }
}

function mostrar(div) {
    obj = document.getElementById(div);
    $(obj).slideToggle();
    //obj.style.display = (obj.style.display == 'none') ? 'block' : 'none';
}

function getAjax(reset=false) {
    if(reset){
        resetFilter();
    }
    if(!newFilter && $("#end").length!=0){
        return; 
    }
    var checkboxes = document.getElementsByClassName("codeFilterCheckBoxInput");
    var i;
    var count = false;
    var send;
    for (i = 0; i<checkboxes.length;i++){
        if(checkboxes[i].checked){
            if(count){
                send = send + "&lang"+i+"="+checkboxes[i].value;
            }else{
                send = "lang"+i+"="+checkboxes[i].value;
                count = !count;
            }
        }
    }
    if(document.getElementById("search").value!="")
        send = send+"&search="+document.getElementById("search").value;
    send = send+"&o="+o;
    doAjax(send);
}


/*
 * Gestión de cookies.
 */
function obtenerCookie(clave) {
    var name = clave + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    }
    return "";
}

function comprobarCookie(clave) {
    var clave = obtenerCookie(clave);
    if (clave!="") {
        return true;
    }else{
        return false;
    }
}

function crearCookie(clave, valor, diasexpiracion) {
    var d = new Date();
    d.setTime(d.getTime() + (diasexpiracion*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = clave + "=" + valor + "; " + expires;
}

function borrarCookie(clave){
    crearCookie(clave,"",-1);
}

function unselectAll() {
    var checkboxes = document.getElementsByClassName("codeFilterCheckBoxInput");
    var i;
    for (i = 0; i<checkboxes.length;i++){
        if(checkboxes[i].type == "checkbox")
            checkboxes[i].checked = false;
        else{
            checkboxes[i].onclick = selectAll;
            checkboxes[i].innerHTML = "Select all";
        }
    }
    getAjax(true);
}

function selectAll() {
    var checkboxes = document.getElementsByClassName("codeFilterCheckBoxInput");
    var i;
    for (i = 0; i<checkboxes.length;i++){
        if(checkboxes[i].type == "checkbox")
            checkboxes[i].checked = true;
        else{
            checkboxes[i].onclick = unselectAll;
            checkboxes[i].innerHTML = "Unselect all";
        }
    }
    getAjax(true);
}

function resetOffset(){
    o=0;
}

function resetFilter(){
    newFilter=true;
    resetOffset();
}

o=1;

$(document.body).on('touchmove', onScroll);
$(window).on('scroll', onScroll);

function onScroll() {
    if (Math.ceil($(window).scrollTop() + $(window).height()) == Math.ceil($(document).height())) {
        getAjax();
    }
}

 //From https://stackoverflow.com/questions/35297919/javascript-select-all-text-inside-a-pre-code-block-on-double-click
window.onload = function(){

    if(!comprobarCookie('cookieconsent_status') || obtenerCookie('cookieconsent_status')=='deny'){
        deleteCookies();
    }

    document.body.addEventListener('dblclick', function(e){
        var target = e.target || e.srcElement;
        if (target.className.indexOf("highlight") !== -1 || target.parentNode.className.indexOf("highlight") !== -1){
            var range, selection;

            if (document.body.createTextRange) {
                range = document.body.createTextRange();
                range.moveToElementText(target);
                range.select();
            } else if (window.getSelection) {
                selection = window.getSelection();
                range = document.createRange();
                range.selectNodeContents(target);
                selection.removeAllRanges();
                selection.addRange(range);
            }
            e.stopPropagation();
        }

    });
};

function deleteCookies() {
    var theCookies = document.cookie.split(';');
    for (var i = 0 ; i < theCookies.length; i++) {
        document.cookie = theCookies[i].split('=')[0] + '=; path=/; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    }
}