function likeAndSave(m,c,l,i=null){
    var b=$('#'+m+"-"+c+"-"+l);
    var data = {};
    switch (m) {
        case 'l':
            if(b.hasClass('fas')){
                data.mode = "unvote";
                data.before = 'fas';
                data.after = 'far';
            }else{
                data.mode = "vote";
                data.before = 'far';
                data.after = 'fas';
            }
            break;
        case 's':
            if(b.hasClass('fas')){
                data.mode = "unsave";
                data.before = 'fas';
                data.after = 'far';
            }else{
                data.mode = "save";
                data.before = 'far';
                data.after = 'fas';
            }
            break;
    }
    if (i==null){
        window.location.replace('/login')
    }

    data.idc=c;
    data.lang=l;

    $.ajax({
        url: '/vote-save',
        type: 'POST',
        contentType: "application/json",
        data: JSON.stringify(data),
        traditional: true,
        success: function(result) {
            console.log(result);
            if (result == 0){
                b.addClass(data.after);
                b.removeClass(data.before);
            }
            else
                myAlertBottom();

        },
        error: function (result) {
            myAlertBottom();
        }
    });
}

function myAlertTop(){
    $(".myAlert-top").fadeToggle();
    setTimeout(function(){
        $(".myAlert-top").fadeToggle();
    }, 2000);
}

function myAlertBottom(){
    $(".myAlert-bottom").fadeToggle();
    setTimeout(function(){
        $(".myAlert-bottom").fadeToggle();
    }, 2000);
}
