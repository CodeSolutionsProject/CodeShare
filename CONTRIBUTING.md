Contribution Guidelines
======================
---------------------
### To add functionality
* Fork the repository
* Add functionality in code
* Comment the functionality with the next format
    ```php
    /* Functionality: <Description> */
    ```
    
### To fix a bug
* Fork the repository
* Fix the bug
* Comment the functionality with the nex format
    ```php
    /* Bug fixed: <Description of bug> - <Description for fixed>*/
    ```

***Do not forget add a clear summary in commit message***

------------------

**If you want optimize the code but not change the normal operation of the software will only have to specify in the commit message which method or snippet you have optimized**